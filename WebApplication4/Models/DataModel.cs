﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using WebApplication4.Interfaces;

namespace WebApplication4.Models
{
    public class DataModel : IDatabase
    {
        public static ConcurrentDictionary<int, string> dataBase = new ConcurrentDictionary<int, string>();

        public string Get(int key)
        {
            dataBase.TryGetValue(key, out string value);
            return value;
        }

        public List<string> Get()
        {
            return dataBase.Values.ToList();
        }

        public string AddOrUpdate(int key, string value)
        {
            dataBase.AddOrUpdate(
              key,
              value,
              (index, oldValue) => value);

            dataBase.TryGetValue(key, out string data);
            return data;
        }

        public string Delete(int key)
        {
            dataBase.TryRemove(key, out string data);
            return data;
        }       
    }
}