﻿using System.Net.Http.Headers;
using System.Web.Http;
using WebApplication4.App_Start;
using WebApplication4.Controllers;
using WebApplication4.Models;

namespace WebApplication4
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.Insert(0, new TextMediaTypeFormatter());
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}
