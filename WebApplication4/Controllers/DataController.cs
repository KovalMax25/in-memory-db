﻿using System.Collections.Generic;
using System.Web.Http;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    [Route("api/data")]
    public class DataController : ApiController
    {
        private DataModel _data;

        public DataController(DataModel db)
        {
            _data = db;
        }

        [HttpGet]
        public List<string> Get()
        {
           return _data.Get();
        }

        [HttpGet]
        [Route("api/data/{id}")]
        public string Get(int id)
        {
            return _data.Get(id);
        }

        [HttpPut]
        [Route("api/data/{id}")]
        public string Put([FromBody] string value, int id)
        {
            return _data.AddOrUpdate(id, value);
        }

        [HttpDelete]
        [Route("api/data/{id}")]
        public string Delete(int id)
        {
            return _data.Delete(id);
        }
    }
}