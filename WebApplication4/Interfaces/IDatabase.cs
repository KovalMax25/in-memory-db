﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication4.Interfaces
{
    public interface IDatabase
    {
        string AddOrUpdate(int key, string value);

        string Delete(int key);

        string Get(int key);
    }
}